package RPGbart.heroes;

//class that defines a hero, it is used to extend: warrior, mage, ranger, rogue classes
public abstract class hero implements Attack {

    //Atributes

    //used to store the hero name
    private String heroName;

    //used to store the hero class
    private String heroClass;

    //used to store the hero level
    public int heroLvl;

    //used to store the hero damage
    public int damage;

    //used to store the hero speed
    public int speed;

    //used to store the hero strength
    public int heroStrength;

    //used to store the hero dexterity
    public int heroDexterity;

    //used to store the hero intelligence
    public int heroIntelligence;



    //constructors
   public  hero () {

   }
    //used to create the info: heroName, heroClass, heroLvl, damage, speed, heroStrength, heroDexterity, heroIntelligence. For the Arraylist
    public hero(String heroName, String heroClass, int heroLvl, int damage, int speed, int heroStrength, int heroDexterity, int heroIntelligence) {
        this.heroName = heroName;
        this.heroClass = heroClass;
        this.heroLvl = heroLvl;
        this.damage = damage;
        this.speed = speed;
        this.heroStrength = heroStrength;
        this.heroDexterity = heroDexterity;
        this.heroIntelligence = heroIntelligence;
    }


    // Getters and Setters

    // enables to get heroName information
    public String getHeroName() {
        return heroName;
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    // enables to get heroClass information
    public String getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(String heroClass) {
        this.heroClass = heroClass;
    }

    // enables to get heroLvl information
    public int getHeroLvl() {
        return heroLvl;
    }

    public void setHeroLvl(int heroLvl) {
        this.heroLvl = heroLvl;
    }

    // enables to get damage information
    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    // enables to get speed information
    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    // enables to get heroStrength information
    public int getHeroStrength() {
        return heroStrength;
    }

    public void setHeroStrength(int heroStrength) {
        this.heroStrength = heroStrength;
    }

    // enables to get heroDexterity information
    public int getHeroDexterity() {
        return heroDexterity;
    }

    public void setHeroDexterity(int heroDexterity) {
        this.heroDexterity = heroDexterity;
    }

    // enables to get heroIntelligence information
    public int getHeroIntelligence() {
        return heroIntelligence;
    }

    public void setHeroIntelligence(int heroIntelligence) {
        this.heroIntelligence = heroIntelligence;
    }


//method gives the hero an attack and can level up

    @Override
    //creates a name, so it can be called for in the TotallAttributes java class
    public void Attack() {
        System.out.println("normal attack");
    }
    @Override
    //creates a name, so it can be called for in the TotallAttributes java class
    public void LevelUp() {
        //gives the character a level and bonus: Strength, Dexterity, Intelligence
        this.heroLvl += 1;
        this.heroStrength += 1;
        this.heroDexterity += 1;
        this.heroIntelligence +=1;
    }
}