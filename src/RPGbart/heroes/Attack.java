package RPGbart.heroes;

//the interface is used to define methods for all the classes
public interface Attack {

   //used to call Attack in TotalAttributes
    void Attack();


   //weapons, used to call all weapons in TotalAttributes
   void Axes() throws Exception;
   void Bows() throws Exception;
   void Daggers() throws Exception;
   void Hammers() throws Exception;
   void Staffs() throws Exception;
   void Swords() throws Exception;
   void Wands() throws Exception;

   //Armor, used to call all Armor in TotalAttributes
   void Cloth() throws Exception;
   void Leather() throws Exception;
   void Mail() throws Exception;
   void Plate() throws Exception;

   //level up, used to call LevelUp in TotalAttributes
    public void LevelUp();
}
