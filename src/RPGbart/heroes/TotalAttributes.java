package RPGbart.heroes;

import java.util.ArrayList;


    public class TotalAttributes {

        //Attributes

        //Main function with an exception to throw if your not the right class or level
        public static void main(String[] args) throws Exception {
            //ArrayList used to get String and Int information from the heroClasses and given the base PrimaryAttributes
            ArrayList<hero> heroes = new ArrayList<>();
            heroes.add(new mage("mage Monzia", "mage", 1, 0, 0, 1, 1, 8));
            heroes.add(new ranger("ranger Rick", "ranger", 1, 0, 0, 1, 7, 1));
            heroes.add(new rogue("rogue Rico", "rogue", 1, 0, 0, 2, 6, 1));
            heroes.add(new warrior("warrior Wario", "warrior", 1, 0, 0, 5, 2, 1));



            //Tests, testing out if everything works

            //shows the information form the arraylist before leveling up or an item is equipped
            System.out.println("Hero Name: " + heroes.get(2).getHeroName());
            System.out.println("HeroLvl: " + heroes.get(2).getHeroLvl());
            System.out.println("HeroClass " + heroes.get(2).getHeroClass());
            System.out.println("Strength: " + heroes.get(2).getHeroStrength());
            System.out.println("Dexterity: " + heroes.get(2).getHeroDexterity());
            System.out.println("Intelligence: " + heroes.get(2).getHeroIntelligence());

            //Gives the hero the levelUp based on the class
            heroes.get(2).LevelUp();

            //Checks if the class and level confirms the requirements if correct, it gives the damage and speed bonus. if not it throws the exception
            heroes.get(2).Swords();

            //Will show the character Damage per second
            heroes.get(2).Attack();

            //shows the Total Attributes now the sword is equipped and a level is gained
            System.out.println("Hero Name: " + heroes.get(2).getHeroName());
            System.out.println("HeroLvl: " + heroes.get(2).getHeroLvl());
            System.out.println("HeroClass " + heroes.get(2).getHeroClass());
            System.out.println("Strength: " + heroes.get(2).getHeroStrength());
            System.out.println("Dexterity: " + heroes.get(2).getHeroDexterity());
            System.out.println("Intelligence: " + heroes.get(2).getHeroIntelligence());

            //Checks if the class and level confirms the requirements if correct, it gives the bonus Attributes are given. if not it throws the exception
            heroes.get(2).Mail();

            //shows the Total Attributes with a extra mail equipped
            System.out.println("Hero Name: " + heroes.get(2).getHeroName());
            System.out.println("HeroLvl: " + heroes.get(2).getHeroLvl());
            System.out.println("HeroClass " + heroes.get(2).getHeroClass());
            System.out.println("Strength: " + heroes.get(2).getHeroStrength());
            System.out.println("Dexterity: " + heroes.get(2).getHeroDexterity());
            System.out.println("Intelligence: " + heroes.get(2).getHeroIntelligence());

            heroes.get(2).Attack();

        }
    }
