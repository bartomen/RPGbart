package RPGbart.heroes.items;


import java.util.List;

public class itemAtrributes {

    private String name;
    private List<items> items;



    //Constructors
    public itemAtrributes(String name, List<items> items) {
        this.name = name;
        this.items = items;
    }

    //Getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<items> getItems() {
        return items;
    }

    public void setItems(List<items> items) {
        this.items = items;
    }
}
