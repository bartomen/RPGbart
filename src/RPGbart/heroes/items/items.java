package RPGbart.heroes.items;

import java.util.ArrayList;


public abstract class items implements Equip {

    //Atributes
    private String itemName;

    public int requiredLvl;
    public int itemDamage;
    public int itemSpeed;
    public int itemStrength;
    public int itemDexterity;
    public int itemIntelligence;


    //Constructors
    public items() {
    }

    public items(String itemName, int requiredLvl, int itemDamage, int itemSpeed, int itemStrength, int itemDexterity, int itemIntelligence) {
        this.itemName = itemName;
        this.requiredLvl = requiredLvl;
        this.itemDamage = itemDamage;
        this.itemSpeed = itemSpeed;
        this.itemStrength = itemStrength;
        this.itemDexterity = itemDexterity;
        this.itemIntelligence = itemIntelligence;
    }

    //getters and setters

    public String getitemName() {
        return itemName;
    }

    public void setitemName(String itemName) {
        this.itemName = itemName;
    }

    public int getRequiredLvl() {
        return requiredLvl;
    }

    public void setRequiredLvl(int requiredLvl) {
        this.requiredLvl = requiredLvl;
    }

    public int getitemDamage() {
        return itemDamage;
    }

    public void setitemDamage(int itemDamage) {
        this.itemDamage = itemDamage;
    }

    public int getitemSpeed() {
        return itemSpeed;
    }

    public void setitemSpeed(int itemSpeed) {
        this.itemSpeed = itemSpeed;
    }

    public int getitemStrength() {
        return itemStrength;
    }

    public void setitemStrength(int itemStrength) {
        this.itemStrength = itemStrength;
    }

    public int getitemDexterity() {
        return itemDexterity;
    }

    public void setitemDexterity(int itemDexterity) {
        this.itemDexterity = itemDexterity;
    }

    public int getitemintelligence() {
        return itemIntelligence;
    }

    public void setitemIntelligence(int itemIntelligence) {
        this.itemIntelligence = itemIntelligence;
    }

    //methods
    @Override
    public void weapon() {
        this.itemDamage += 10;
        this.itemSpeed += 2;
    }
}