package RPGbart.heroes.items;

public abstract class weapons {

    //Atributes
    private String name;
    public int requiredlvl;
    boolean isWearable = false;
    public String wearloc;

    //Constructors
    public weapons (){

    }

    public weapons(String name, int requiredlvl, boolean isWearable, String wearloc) {
        this.name = name;
        this.requiredlvl = requiredlvl;
        this.isWearable = isWearable;
        this.wearloc = wearloc;
    }

    //getters en setters


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequiredlvl() {
        return requiredlvl;
    }

    public void setRequiredlvl(int requiredlvl) {
        this.requiredlvl = requiredlvl;
    }

    public boolean isWearable() {
        return isWearable;
    }

    public void setWearable(boolean wearable) {
        isWearable = wearable;
    }

    public String getWearloc() {
        return wearloc;
    }

    public void setWearloc(String wearloc) {
        this.wearloc = wearloc;
    }
}