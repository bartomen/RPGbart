package RPGbart.heroes.items;

public class staffs extends weapons {
    //Attributes
    private int damage;
    private int speed;

    //constructors
    public staffs() {
        super();
    }

    public staffs (String name, int requiredlvl, boolean isWearable, String wearloc, int damage, int speed) {
        super(name, requiredlvl, false, wearloc);
        this.damage = damage;
        this.speed = speed;
    }

    //getters & setters


    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}

