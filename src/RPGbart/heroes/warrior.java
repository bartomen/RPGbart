package RPGbart.heroes;

import RPGbart.heroes.items.Equip;
import RPGbart.heroes.items.items;

//defines warrior class
public class warrior extends hero{


    //Constructors
    public warrior() {

    }

    //Used to store warriors stats so it can be used in the ArrayList
    public warrior(String heroName, String heroClass, int heroLvl, int damage, int speed, int heroStrength, int heroDexterity, int heroIntelligence) {
        super(heroName, heroClass, heroLvl, damage, speed, heroStrength, heroDexterity, heroIntelligence);
    }



    //methods used to add weapons, Armor, level up and an attack

    //Attack, shows how much DPS a character does
    @Override
    //creates a name, so it can be called for in the TotallAttributes java class
    public void Attack() {
    // creates character dps output
    double dps = (damage + (1 + heroStrength/100)) *speed;
                System.out.println("Character DPS: " + dps + " Damage per second");
    }


    //weapons, gives the character: damage and speed bonus
        @Override
        //creates a name, so it can be called for in the TotallAttributes java class
        public void Axes() throws Exception {

            //checks if the character is high enough level
        if (2 > heroLvl) {
            throw new Exception("Your Level is to low");
        }
            //checks if the character is the right class
        if (getHeroClass() != "warrior")
            throw new Exception("Your the wrong Class");
        else {
            //gives damage and speed bonus
            this.damage += 20;
            this.speed += 2;
            System.out.println("Axe equip");
        }
    }

        @Override
        //creates a name, so it can be called for in the TotallAttributes java class
        public void Bows() throws Exception {
            //checks if the character is high enough level
            if (2 > heroLvl) {
                throw new Exception("Your Level is to low");
            }
            //checks if the character is the right class
            if (getHeroClass() != "ranger")
                throw new Exception("Your the wrong Class");
            else {
                //gives damage and speed bonus
                this.damage += 20;
                this.speed += 2;
                System.out.println("bow equip");
            }
        }

        @Override
        //creates a name, so it can be called for in the TotallAttributes java class
        public void Daggers() throws Exception {

            //checks if the character is high enough level
            if (2 > heroLvl) {
                throw new Exception("Your Level is to low");
            }
            //checks if the character is the right class
            if (getHeroClass() != "rogues")
                throw new Exception("Your the wrong Class");
            else {
                //gives damage and speed bonus
                this.damage += 20;
                this.speed += 2;
                System.out.println("dagger equip");
            }
        }
        @Override
        //creates a name, so it can be called for in the TotallAttributes java class
        public void Hammers() throws Exception {
            //checks if the character is high enough level
            if (2 > heroLvl) {
                throw new Exception("Your Level is to low");
            }
            //checks if the character is the right class
            if (getHeroClass() != "warrior")
                throw new Exception("Your the wrong Class");
            else {
                //gives damage and speed bonus
                this.damage += 20;
                this.speed += 2;
                System.out.println("hammer equip");
            }
        }

         @Override
         //creates a name, so it can be called for in the TotallAttributes java class
         public void Staffs() throws Exception {
             //checks if the character is high enough level
            if (2 > heroLvl) {
                throw new Exception("Your Level is to low");
            }
             //checks if the character is the right class
            if (getHeroClass() != "mage")
                throw new Exception("Your the wrong Class");
            else {
                //gives damage and speed bonus
                this.damage += 20;
                this.speed += 2;
                System.out.println("staff equip");
            }
        }

        @Override
        //creates a name, so it can be called for in the TotallAttributes java class
        public void Swords() throws Exception {
            //checks if the character is high enough level
            if (2 > heroLvl) {
                throw new Exception("Your Level is to low");
            }
            //checks if the character is the right class
            if (getHeroClass() != "warrior" && getHeroClass() != "rogue")
                throw new Exception("Your the wrong Class");
            else {
                //gives damage and speed bonus
                this.damage += 20;
                this.speed += 2;
                System.out.println("sword equip");
            }
        }

        @Override
        //creates a name, so it can be called for in the TotallAttributes java class
        public void Wands() throws Exception {
            //checks if the character is high enough level
            if (2 > heroLvl) {
                throw new Exception("Your Level is to low");
            }
            //checks if the character is the right class
            if (getHeroClass() != "mage")
                throw new Exception("Your the wrong Class");
            else {
                //gives damage and speed bonus
                this.damage += 20;
                this.speed += 2;
                System.out.println("wand equip");
            }
        }

    //Armor, gives the character: strength, dexterity and intelligence bonus


    @Override
    //creates a name, so it can be called for in the TotallAttributes java class
    public void Cloth() throws Exception {
        //checks if the character is high enough level
        if (2 > heroLvl) {
            throw new Exception("Your Level is to low");
        }
        //checks if the character is the right class
        if (getHeroClass() != "mage")
            throw new Exception("Your the wrong Class");
        //gives the character bonus: Strength, Dexterity, Intelligence
        else {
            //gives damage and speed bonus
            this.heroStrength += 0;
            this.heroDexterity += 0;
            this.heroIntelligence += 500;
            System.out.println("cloth equip");
        }
    }

    @Override
    //creates a name, so it can be called for in the TotallAttributes java class
    public void Leather() throws Exception {
        //checks if the character is high enough level
        if (2 > heroLvl) {
            throw new Exception("Your Level is to low");
        }
        //checks if the character is the right class
        if (getHeroClass() != "ranger" && getHeroClass() != "rogue")
            throw new Exception("Your the wrong Class");
        else {
            //gives the character bonus: Strength, Dexterity, Intelligence
            this.heroStrength += 0;
            this.heroDexterity += 480;
            this.heroIntelligence += 0;
            System.out.println("leather equip");
        }
    }

    @Override
    //creates a name, so it can be called for in the TotallAttributes java class
    public void Mail() throws Exception {
        //checks if the character is high enough level
        if (2 > heroLvl) {
            throw new Exception("Your Level is to low");
        }
        //checks if the character is the right class
        if (getHeroClass() != "ranger" && getHeroClass() != "rogue" && getHeroClass() != "warrior")
            throw new Exception("Your the wrong Class");
        else {
            //gives the character bonus: Strength, Dexterity, Intelligence
            this.heroStrength += 300;
            this.heroDexterity += 300;
            this.heroIntelligence += 50;
            System.out.println("mail equip");
        }
    }

    @Override
    //creates a name, so it can be called for in the TotallAttributes java class
    public void Plate() throws Exception {
        //checks if the character is high enough level
        if (2 > heroLvl) {
            throw new Exception("Your Level is to low");
        }
        //checks if the character is the right class
        if (getHeroClass() != "warrior")
            throw new Exception("Your the wrong Class");
        else {
            //gives the character bonus: Strength, Dexterity, Intelligence
            this.heroStrength += 450;
            this.heroDexterity += 100;
            this.heroIntelligence += 0;
            System.out.println("plate equip");
        }
    }


    //Level up
    @Override
    //creates a name, so it can be called for in the TotallAttributes java class
    public void LevelUp() {
        //gives the character a level and bonus: Strength, Dexterity, Intelligence
        this.heroLvl += 1;
        this.heroStrength += 3;
        this.heroDexterity += 2;
        this.heroIntelligence += 1;
        System.out.println("congratulations, you leveled up");
    }
}
