<h1> Assignment 1: RPG bart Heroes application </h1>

<h2> Table of Contents </h2>

-   Background   
-   Install
-   Usage
-   Maintainers
-   License
-   Authors and acknowledgment

<h2> Background </h2>

In the Role playing game you can create a hero. This hero has a class, each class has its own unique attributes which are indicated with: Strength points, Dexterity points, and Intelligence points. Each class has its own unique leveling system and a selected type of armor and weapons they can wear.

In this game the following classes are:

-	Mage
-	Ranger
-	Rogue
-	Warrior

They can quip the following armor types:

-	Cloth
-	Leather
-	Mail
-	Plate

And they wield the following weapons:

-	Axes
-	Bows
-	Daggers
-	Hammers
-	Staffs
-	Swords
-	Wands



The application is tested, but not 100% covered. 



The source files packages following structure:

Src\RPGbart\heroes
- Attack.java
- TotalAttributes.java
- hero.java
- mage.java
- ranger.java
- rogue.java
- warrior.java
- items (map)


<h2> Install </h2>

-	Install JDK 17
-	Install Intellij
-	Clone repository

<h2> Usage </h2>

-	Run the test by right clicking ‘tests’ or by selecting run ‘All Tets’.

<h2> Maintainers </h2>

@bartomen

<h2> License </h2>

The project is open-source. Feel free to use it in your own projects, as long as credit the work.

<h2> Authors and acknowledgment </h2>
Code author: Bart van Dongen

Assignment given by: Livinus Obiora Nweke, Lecturer at Noroff University College
